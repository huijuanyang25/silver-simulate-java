package com.tw;

public class ParkingBoy {
    private final ParkingLot parkingLot;
    private String lastErrorMessage;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingTicket park(Car car) {
        // TODO: Implement the method according to test
        // <-start-
        ParkingResult parkingresult = parkingLot.park(car);
        if (!parkingresult.isSuccess()) {
            lastErrorMessage = parkingresult.getMessage();
        } else {
            return parkingresult.getTicket();
        }
        return parkingresult.getTicket();
        // ---end->
    }

    public Car fetch(ParkingTicket ticket) {
        // TODO: Implement the method according to test
        // <-start-
        FetchingResult fetchingresult = parkingLot.fetch(ticket);
        if (!fetchingresult.isSuccess()) {
            lastErrorMessage = fetchingresult.getMessage();
        } else {
            return fetchingresult.getCar();
        }
        return fetchingresult.getCar();
        // ---end->
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
}
